
package at.fhooe.sms.security;


public interface EnDeCrypter {

    /**
     * Encrypt a string that is being send over an unsecure channel.
     *
     * @param original The data that is to be encrypted.
     * @return An encrypted version of the original data.
     */
    String encrypt(String original);

    /**
     * Undo the encryption done to the original data by the encrypt() method.
     *
     * @param encrypted The encrypted data.
     * @return The original data.
     * @throws ValidationException Optionally thrown if a data integrity check fails.
     */
    String decrypt(String encrypted) throws ValidationException;
}
