
package at.fhooe.sms.security;

/**
 * Indicates that an error occurred while validating the integrity of data managed by an
 * {@link EnDeCrypter}.}
 */
public class ValidationException extends Exception {
    public ValidationException() {
      super();
    }

    public ValidationException(String s) {
      super(s);
    }

    private static final long serialVersionUID = 1L;
}
